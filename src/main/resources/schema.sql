CREATE TABLE IF NOT EXISTS Salary (
                           id INT AUTO_INCREMENT PRIMARY KEY,
                           employment VARCHAR(255) NOT NULL,
                           annual_salary BIGINT
);

CREATE TABLE IF NOT EXISTS Employee (
                           id INT AUTO_INCREMENT PRIMARY KEY,
                           name VARCHAR(255) NOT NULL,
                           email VARCHAR(255) NOT NULL,
                           salary_id INT,
                           CONSTRAINT fk_salary FOREIGN KEY (salary_id) REFERENCES Salary(id)
);