package com.example.thymeleaf_employee.services;

import com.example.thymeleaf_employee.domain.Salary;

import java.util.List;

public interface ISalaryService {
    List<Salary> getAllSalaries();

    Salary getSalaryById(Long id);
}
