package com.example.thymeleaf_employee.services;

import com.example.thymeleaf_employee.domain.Employee;
import com.example.thymeleaf_employee.repos.EmployeeRepository;
import com.example.thymeleaf_employee.repos.SalaryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService implements IEmployeeService {
    private final EmployeeRepository employeeRepository;
    private final SalaryRepository salaryRepository;

    @Autowired
    public EmployeeService(EmployeeRepository employeeRepository, SalaryRepository salaryRepository) {
        this.employeeRepository = employeeRepository;
        this.salaryRepository = salaryRepository;
    }

    @Override
    public List<Employee> getAllEmployee() {
        return employeeRepository.findAll();
    }

    @Override
    public void save(Employee employee) {
        employeeRepository.save(employee);
    }

    @Override
    public Employee getById(Long id) {
        return employeeRepository.findById(id).get();
    }

    @Override
    public void deleteViaId(Long id) {
        employeeRepository.deleteById(id);
    }

    @Override
    public boolean isEmailExisting(String email) {
        return employeeRepository.existsByEmail(email);
    }
}
