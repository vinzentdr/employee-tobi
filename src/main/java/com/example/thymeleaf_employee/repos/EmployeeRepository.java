package com.example.thymeleaf_employee.repos;

import com.example.thymeleaf_employee.domain.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    boolean existsByEmail(@NonNull String email);
}