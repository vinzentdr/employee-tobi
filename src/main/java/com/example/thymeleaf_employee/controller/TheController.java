package com.example.thymeleaf_employee.controller;

import com.example.thymeleaf_employee.domain.Employee;
import com.example.thymeleaf_employee.services.EmployeeService;
import com.example.thymeleaf_employee.services.SalaryService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class TheController {
    private final EmployeeService employeeService;
    private final SalaryService salaryService;

    @Autowired
    public TheController(EmployeeService employeeService, SalaryService salaryService) {
        this.employeeService = employeeService;
        this.salaryService = salaryService;
    }

// "/employee-list"
// "/showFormForAddEmployee"
// "/save"
// "/updateEmployee"
// "/showFormForUpdate/{id}"
// "/deleteEmployee/{id}"
// "/salaries"

    @GetMapping("employees/list")
    private String getAllEmployees(Model model) {
        model.addAttribute("employees", employeeService.getAllEmployee());
        return "employee-list";
    }


    @GetMapping("salaries/list")
    private String getAllSalaries(Model model) {
        model.addAttribute("salaries", salaryService.getAllSalaries());
        return "salary-list";
    }


    @GetMapping("employees/newEmployee")
    private String getNewEmployee(Model model) {
        model.addAttribute("employee", new Employee());
        model.addAttribute("salaries", salaryService.getAllSalaries());
        return "new-employee";
    }

    @PostMapping("employees/newEmployee")
    private String postNewEmployee(@Valid Employee employee, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("employee", employee);
            model.addAttribute("salaries", salaryService.getAllSalaries());
            return "new-employee";
        }
        employeeService.save(employee);
        return "redirect:/employees/list";
    }

    @GetMapping("employees/updateEmployee/{id}")
    private String getUpdateEmployee(@PathVariable Long id, Model model) {
        model.addAttribute("employee", employeeService.getById(id));
        model.addAttribute("salaries", salaryService.getAllSalaries());
        return "update-employee";
    }

    @PostMapping("employees/updateEmployee")
    private String postUpdateEmployee(@Valid Employee employee, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("employee", employee);
            model.addAttribute("salaries", salaryService.getAllSalaries());
            return "update-employee";
        }
        employeeService.save(employee);
        return "redirect:/employees/list";
    }
}
