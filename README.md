# Employee Management System
![img.png](src/main/resources/images/img.png)

# Given Components
* SQL Schema.sql
* SQL data.sql
* Application Properties -> **h2 database**
* pom.xml
* Static HTML Content -> Refer **http://address:port/list**
* Bootstrap css file
* HTML Help
  * index.html is also linking to the help pages 
* Package declarations
* Interfaces for all Service classes

# 1. Die Entitäten - Beziehungsstatus: Bidirectional 1:n
* Klasse Employee:
 id, name, email, salary (db_name:salary_id)
* Klasse Salary:
 id, employment, annualSalary, List\<Employees\>

# 2. Repositories
* Erzeugen Sie die notwendigen Repositories.

# 3. Service Klassen
* Erzeugen Sie eine EmployeeService sowie eine SalaryService Klasse, welche das jeweilige gegebene Interface implementiert. 
* Implementieren Sie die vorgegebenen Methoden.

# 4. TheController
* Schreiben Sie einen Controller, welcher folgende Endpoints unterstützt:
  * /employee-list
    * Liefert alle Employees wie im Bild 1 
  * /showFormForAddEmployee
    * Liefert die Seite **newemployee.html**
  * /save
    * Speichert einen Angestellten 
  * /updateEmployee
    * Verändert einen Angestellten
  * /showFormForUpdate/{id}
    *  Zeigt die Update-Seite
  * /deleteEmployee/{id}
    * Löscht den Angestellten mit der Id 
  * /salaries
    * Zeigt alle Gehälter

# 5. The Web
Es sollen 4 HTML Seiten bereitgestellt werden:
* employee-list.html -> Bild 1
* new-employee.html -> Bild 2
* update-employee.html -> Bild 3
* salary-list.html -> Bild 4

# 6. Test
Es gibt Tests mit denen Sie Ihr Programm testen können.
Die Testfälle bilden nicht alles ab!

# 7. Bonus
* Eingabeabsicherung 
  * Texte dürfen nicht leer sein
  * Die Email Adresse muss auch eine sein 
* Keine doppelten EMail Adressen
  * new user
  * update user

# HTML HELP
* Forms: [help_forms.html](src/main/resources/static/help/help_forms.html)
* Select / Buttons: [help_select_buttons.html](src/main/resources/static/help/help_select_buttons.html)
* Tables: [help_tables.html](src/main/resources/static/help/help_tables.html)



# Wie es aussehen sollte:
![img_1.png](src/main/resources/images/img_1.png)
![img_2.png](src/main/resources/images/img_2.png)
![img_3.png](src/main/resources/images/img_3.png)
![img_4.png](src/main/resources/images/img_4.png)

